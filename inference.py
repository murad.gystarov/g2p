import yaml
import numpy as np
import tensorflow as tf

from graphs.deep_blstm import create_blstm
from data.vocabs import GRAPHEMES, PHONEMES
from data_prepare import get_sequence_one_hots


def get_inputs(words, tokens_gr):
    inputs = []
    for word in words:
        graphemes = list(word)
        inputs.append(get_sequence_one_hots(graphemes, tokens_gr))
    return np.array(inputs, dtype=np.float32)


def get_spells(predicts, tokens_ph):
    spells = []
    for sequence in predicts:
        phonemes = []
        for phoneme_distrib in sequence:
            phonemes.append(tokens_ph[phoneme_distrib.argmax()])
        spells.append(''.join(phonemes))
    return spells


if __name__ == '__main__':
    experiment_number = '1'
    with open('experiments/exp_1/metrics.yaml', 'r') as f:
        metrics = yaml.load(f, Loader=yaml.FullLoader)
    with open('experiments/exp_1/config_deep_blstm.yaml', 'r') as f:
        config_model = yaml.load(f, Loader=yaml.FullLoader)
    checkpoint_dir = f'experiments/exp_{experiment_number}/ckpt'
    model = create_blstm(config_model)

    path_to_checkpoint = tf.train.latest_checkpoint(checkpoint_dir, latest_filename=None)
    path = f'{checkpoint_dir}/ckpt-4'
    ckpt = tf.train.Checkpoint(model=model)
    ckpt.restore(path)
    model.load_weights(path)
    examples = ['привет']
    inputs = get_inputs(examples, GRAPHEMES)
    predicts = model.predict(inputs)
    spells = get_spells(predicts, PHONEMES)
    print(spells)
