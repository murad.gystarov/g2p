import os
import yaml
import time
from datetime import date
from datetime import datetime

import tensorflow as tf
import numpy as np
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow import nn
import matplotlib.pyplot as plt


from test import prepare_test_data, func_testing
from data.vocabs import PHONEMES, GRAPHEMES
from graphs.simple_rnn import create_simple_rnn
from graphs.lstm import create_lstm
from graphs.deep_blstm import create_blstm
from inference import get_inputs, get_spells

gpus = tf.config.list_physical_devices('GPU')
if len(gpus) >= 2:
    tf.config.set_visible_devices(gpus[1], 'GPU')
elif len(gpus) == 1:
    tf.config.set_visible_devices(gpus[0], 'GPU')


def train_model(model, train_dataset, val_dataset, epochs, test_config,
                log_file, test_file, ckpt, ckpt_manager, load_pretrained_model,
                optimizer, loss_fn):
    if load_pretrained_model:
        ckpt.restore(ckpt_manager.latest_checkpoint)  # восстанавливает контрольную точку обучения
        if ckpt_manager.latest_checkpoint:
            log_file.write("[TRAIN] Checkpoint restored from {}\n".format(ckpt_manager.latest_checkpoint))
            log_file.write(f"[TRAIN] Optimizer params: {optimizer.get_config()}\n")
        else:
            log_file.write(f'[TRAIN] Initialize from scratch.')
    sum_loss_epoch = 0
    count_epoch = 0
    # best_avg_val_loss = 1.0
    X, y = prepare_test_data(test_config['path_test_data'])
    amount_steps = len([1 for _ in train_dataset])
    train_info = f"amount epochs={epochs}; amount steps per epoch={amount_steps}"
    log_file.write(f'{train_info}\n')
    for epoch in range(epochs):
        start_time = time.time()
        string_epoch = "\nStart of epoch %d\n" % (epoch,)
        log_file.write(string_epoch)
        print(string_epoch)
        sum_loss = 0.0
        count = 0.0
        for step, (x_batch_train, y_batch_train) in enumerate(train_dataset):
            # np_x = np.array(x_batch_train)
            # np_y = np.array(y_batch_train)
            loss_value = train_step(x_batch_train, y_batch_train, optimizer, loss_fn)
            sum_loss += loss_value
            sum_loss_epoch += loss_value
            count += 1
            count_epoch += 1
            if step % 2000 == 0:
                ckpt_save_path = ckpt_manager.save()
                avg_loss = sum_loss / count
                string_step = "step=%d loss=%.4f" % (step, float(avg_loss))
                log_file.write(f'{string_step}\n')
                print(string_step)
                sum_loss = 0.0
                count = 0.0
        val_sum_loss = 0.0
        val_count = 0
        for x_batch_val, y_batch_val in val_dataset:
            y_batch_pred_val = model(x_batch_val, training=False)
            val_loss = loss_fn(y_batch_val, y_batch_pred_val)
            val_sum_loss += val_loss
            val_count += 1
        val_avg_loss = val_sum_loss / val_count
        avg_loss_epoch = sum_loss_epoch / count_epoch
        print(time.time())
        string_epoch_result = "epoch=%d loss=%.4f val_loss=%.4f time_epoch=%.1f" % (epoch, avg_loss_epoch,
                                                                                    val_avg_loss,
                                                                                    time.time() - start_time)
        log_file.write(f'{string_epoch_result}\n')
        print(string_epoch_result)
        func_testing(model, X, y, test_file, PHONEMES, log_file, metrics)
        ckpt_save_path = ckpt_manager.save()
        log_file.write("[TRAINER] Checkpoint saved to {}\n".format(ckpt_save_path))


def train_step(x, y, optimizer, loss_fn):
    with tf.GradientTape() as tape:
        y_batch_pred = model(x, training=False)
        loss_value = loss_fn(y, y_batch_pred)
    grads = tape.gradient(loss_value, model.trainable_weights)
    optimizer.apply_gradients(zip(grads, model.trainable_weights))
    return loss_value


def train_step_ctc(x, y):
    indexes = tf.argmax(y, axis=2)
    label_length = 0  # настоящая длина выходной последовательности decoded_sequence
    logit_length = 0  # предсказываемая длина выходной последовательности predicted_sequence паддится до длины входной
    # последовательности input_sequence=target_sequence=predicted_sequence
    with tf.GradientTape() as tape:
        # y_batch_pred = model(x, training=True)
        logits = model(x, training=True)
        # loss_value = loss_fn(y, y_batch_pred)
        loss_value = nn.ctc_loss(indexes, logits, label_length, logit_length, logits_time_major=False)
    grads = tape.gradient(loss_value, model.trainable_weights)
    optimizer.apply_gradients(zip(grads, model.trainable_weights))
    return loss_value


def plot_history_loss(training_history,
                      image_dest_dir: str = None,
                      show_plot: bool = True):
    plt.plot(training_history.history['loss'], label='Training Loss')
    plt.plot(training_history.history['val_loss'], label='Validation Loss')
    plt.legend()
    plt.xlabel('Epochs')
    plt.ylabel('Mean Squared Error')
    if image_dest_dir:
        plt.savefig(f"{image_dest_dir}/training_loss.png")
    if show_plot:
        plt.show()
    plt.close()


if __name__ == '__main__':
    experiment_number = '1'

    with open(f'experiments/exp_{experiment_number}/train_config.yaml', 'r') as f:
        train_config = yaml.load(f, Loader=yaml.FullLoader)
    with open(f'experiments/exp_{experiment_number}/metrics.yaml', 'r') as f:
        metrics = yaml.load(f, Loader=yaml.FullLoader)
    with open(train_config['path_to_config_model'], 'r') as f:
        config_model = yaml.load(f, Loader=yaml.FullLoader)

    now = datetime.now()
    now_format = now.strftime("%Y_%m_%d_%H:%M")
    log_file = open(f"{train_config['path_to_exp']}/logs/{now_format}.log", 'w', buffering=1)
    test_file = open(f"{train_config['path_to_exp']}/tests/{now_format}.test", 'w', buffering=1)
    print("\nLOAD DATA")
    train_dataset = tf.data.experimental.load(train_config['path_train_dataset'])
    val_dataset = tf.data.experimental.load(train_config['path_val_dataset'])
    batch_size = train_config['batch_size']
    epochs = train_config['epochs']

    print("\nCREATE MODEL")
    model = create_blstm(config_model)
    optimizer = Adam(learning_rate=train_config['learning_rate'])
    loss_fn = CategoricalCrossentropy()
    ckpt = tf.train.Checkpoint(model=model, optimizer=optimizer)
    ckpt_manager = tf.train.CheckpointManager(ckpt,
                                              directory=f'{train_config["path_to_exp"]}/ckpt',
                                              max_to_keep=3)
    for input_example_batch, target_example_batch in train_dataset.take(1):
        # print(input_example_batch)
        example_batch_predictions = model(input_example_batch)
        print(example_batch_predictions.shape, "# (batch_size, sequence_length, vocab_size)")
    model.summary()
    train_model(model, train_dataset, val_dataset, epochs, metrics, log_file,
                test_file, ckpt, ckpt_manager, train_config['load_pretrained_model'],
                optimizer, loss_fn)
    log_file.close()
    test_file.close()

    print("\nSAVING MODEL")
    print(f"   * load last checkpoint")
    model.load_weights(ckpt_manager.latest_checkpoint)  # загрузка лучшего чекпоинта тоесть последнего реализовать
    print(f"   * export to: {train_config['path_to_save_model']}")
    model.save(train_config['path_to_save_model'], save_format="tf")  # мы сохроняем всю модель а не просто веса
    print(f"   * done!")
    print("\nFINISHED!")
